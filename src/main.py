# python script
from api_serivce import ApiService
from file_manager import FileManager
from input_manager import InputManager


def main():
    # InputManager get valid arguments for api call
    input_manager = InputManager()
    args = input_manager.get_input()

    # ApiService get results of api call with valid arguments
    data = ApiService().get_api_data(
        countries_list=args["countries_list"],
        start_date=args["start_date"],
        end_date=args["end_date"],
    )

    # FileManager write data into a files
    file_manager = FileManager(start_date=args["start_date"], end_date=args["end_date"], api_data=data)
    file_manager.write_files()


if __name__ == "__main__":
    main()
