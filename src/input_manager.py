from iso_3166 import county_codes
from datetime import datetime


class InputManager:
    YES_VALUES = ["yes", "y"]

    @staticmethod
    def _validate_country_code(input_str):
        return input_str.upper() in county_codes.values()

    def _get_countries(self):
        while True:
            countries_input = (
                input("Enter country codes (separated by commas) or 'list' to see available codes: ").strip().lower()
            )

            if not countries_input:
                print("Please provide at least one country code.")
                continue

            if countries_input == "list":
                print("Available country codes:", ", ".join(county_codes.values()))
                continue

            country_codes_list = [code.strip() for code in countries_input.split(",")]
            invalid_countries = [code for code in country_codes_list if not self._validate_country_code(code)]

            if invalid_countries:
                print("Invalid country code(s):", ", ".join(invalid_countries))
                print("Please provide valid codes or type 'list' to see available codes.")
            else:
                return country_codes_list

    def _get_date(self, date_type):
        while True:
            date_input = input(f"Enter {date_type} date (YYYY-MM-DD): ").strip()

            if not date_input:
                if date_type == "start":
                    print(f"{date_type} date cannot be empty.")
                    continue
                use_exact_date = (
                    input(f"Do you want to use the exact date for {date_type} date? ({', '.join(self.YES_VALUES)}): ")
                    .strip()
                    .lower()
                )
                if use_exact_date in self.YES_VALUES:
                    return None

            try:
                date = datetime.strptime(date_input, "%Y-%m-%d").date()
            except ValueError:
                print("Invalid date format. Please use YYYY-MM-DD format. Or leave empty.")
                continue

            return date

    def get_input(self):
        while True:
            countries_list = self._get_countries()
            start_date = self._get_date("start")
            end_date = self._get_date("end")

            if end_date and start_date > end_date:
                print("Start date cannot be later than end date.")
            else:
                return {"countries_list": countries_list, "start_date": start_date, "end_date": end_date}
