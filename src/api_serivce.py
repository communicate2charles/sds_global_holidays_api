# API Key: sIrG9sHETBfglCGlsQDKTqPaByANdsSU
# API url: https://calendarific.com/api/v2
# ?api_key=sIrG9sHETBfglCGlsQDKTqPaByANdsSU
# url = f"https://calendarific.com/api/v2/holidays?api_key={API_KEY}&country={COUNTRY}&year={YEAR}"
import os
import requests


class ApiService:
    API_URL = "https://calendarific.com/api/v2/"

    def __init__(self):
        self.api_key = self._get_api_key()

    @staticmethod
    def _get_api_key():
        return os.getenv("API_KEY") or input("Enter your API key for e.x. (sIrG9sHETBfglCGlsQDKTqPaByANdsSU) : ")

    def _build_url(self, start_date, end_date, country_code, endpoint="holidays", optional_args=""):
        if start_date == end_date or (start_date and not end_date):
            optional_args = f"&month={start_date.month}&day={start_date.day}"
        else:
            optional_args = ""

        url = f"{self.API_URL}{endpoint}?api_key={self.api_key}&country={country_code}&year={start_date.year}{optional_args}"
        return url

    def _make_endpoint_call(self, start_date, end_date, country_code):
        url = self._build_url(start_date=start_date, end_date=end_date, country_code=country_code)

        try:
            response = requests.get(url=url)
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            raise Exception(f"API request failed with status code {e.response.status_code}")
        except requests.exceptions.RequestException as e:
            raise Exception(f"API request failed: {str(e)}")

        return response.json()

    def get_api_data(self, countries_list, start_date, end_date):
        result = {}

        for country_code in countries_list:
            result[country_code] = []

            for year in range(start_date.year, end_date.year + 1):
                try:
                    response = self._make_endpoint_call(start_date, end_date, country_code)
                    result[country_code].extend(response["response"]["holidays"])
                except Exception as e:
                    print(f"Error for {country_code}, year {year}: {str(e)}")

        return result


# # Example usage:
# countries_list = ['ua', 'us', 'gb']
# start_date = datetime(year=1992, month=7, day=7)
# end_date = datetime(year=1992, month=9, day=18)
#
# api_service = ApiService()
# call_args = (countries_list, start_date, end_date)
# api_data = api_service.get_api_data(call_args)
