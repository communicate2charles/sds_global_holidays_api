from datetime import datetime
import json


class FileManager:
    def __init__(self, start_date, end_date, api_data):
        self.start_date = start_date
        self.end_date = end_date
        self.data = api_data

    def write_files(self):
        for key, values in self.data.items():
            filtered_values = [value for value in values if self.is_date_in_range(_date=value["date"]["datetime"])]
            if filtered_values:
                file_name = f"{key}_{self.start_date.strftime('%d-%m-%Y')}_{self.end_date.strftime('%d-%m-%Y')}.txt"
                with open(file_name, "w", encoding="utf-8") as file:
                    for value in filtered_values:
                        json.dump(value, file, ensure_ascii=False)
                        file.write("\n")

    def is_date_in_range(self, _date: dict):
        date = datetime(year=_date["year"], month=_date["month"], day=_date["day"]).date()
        return self.start_date <= date <= self.end_date


data = {
    "pl": [
        {
            "name": "New Year's Day",
            "description": "New Year’s Day, on January 1, is the first day of the year and a public holiday in Poland.",
            "country": {"id": "pl", "name": "Poland"},
            "date": {
                "iso": "2002-01-01",
                "datetime": {"year": 2002, "month": 1, "day": 1},
            },
            "type": ["National holiday"],
            "primary_type": "National holiday",
            "canonical_url": "https://calendarific.com/holiday/poland/new-year-day",
            "urlid": "poland/new-year-day",
            "locations": "All",
            "states": "All",
        },
        {
            "name": "Valentine's Day",
            "description": "Valentine’s Day (Walentynki) is one of the most romantic days of the year celebrated in many countries including Poland on February 14.",
            "country": {"id": "pl", "name": "Poland"},
            "date": {
                "iso": "2002-02-14",
                "datetime": {"year": 2002, "month": 2, "day": 14},
            },
            "type": ["Observance"],
            "primary_type": "Observance",
            "canonical_url": "https://calendarific.com/holiday/poland/valentine-day",
            "urlid": "poland/valentine-day",
            "locations": "All",
            "states": "All",
        },
        {
            "name": "March Equinox",
            "description": "March Equinox in Poland (Warsaw)",
            "country": {"id": "pl", "name": "Poland"},
            "date": {
                "iso": "2002-03-20T20:16:08+01:00",
                "datetime": {
                    "year": 2002,
                    "month": 3,
                    "day": 20,
                    "hour": 20,
                    "minute": 16,
                    "second": 8,
                },
                "timezone": {
                    "offset": "+01:00",
                    "zoneabb": "CET",
                    "zoneoffset": 3600,
                    "zonedst": 0,
                    "zonetotaloffset": 3600,
                },
            },
            "type": ["Season"],
            "primary_type": "Season",
            "canonical_url": "https://calendarific.com/holiday/seasons/vernal-equinox",
            "urlid": "seasons/vernal-equinox",
            "locations": "All",
            "states": "All",
        },
    ],
}
